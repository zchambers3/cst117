# CST-117 
This course provides an introduction to the fundamentals of C# programming and the .NET platform. The course covers program design and development, 
debugging techniques, structured and object-oriented programming and basic GUI elements.

## Exercise 1
Install the current version of Visual Studio .NET on your personal computer. Once the software is installed, create a Windows Forms Application and build a form with a variety of controls.

## Exercise 2
Create a repository for this course as directed by your instructor. Connect to the repository and submit your project developed for IC_01.

## Exercise 3
Write a Windows Forms Application that prompts the user to enter a value and then converts the value to some other type of units.

## Programming Project 1
Extend the program written for Exercise 3 by adding exception handling to your program. Further enhance your project by formatting all numeric output to exactly three decimal places.

## Milestone 1
Part 1: Design an initial model for the data in your application. The data model should include at least five applicable properties. Property data types are stated and appropriate, property names are self-documenting.

Part 2: Sketch a low-fidelity prototype (hand-drawing) of the flow of interactions in your application

Part 3: Create a high-fidelity prototype of the flow of interactions in your application using built in design tools supplied in the .NET framework

Deliverables:

A list of the properties, complete with data type and description that will describe an Inventory item in your system. Submit this as a .pdf file.
A low-fidelity prototype of your system’s flow of interactions. This should be originally hand-drawn and submitted as a .pdf file.
A high-fidelity prototype of your system’s interface. This should be developed in Visual Studio using .NET tools. Take a screen shot of all windows and submit as a single .pdf file.
What to submit:

Submit all deliverables as .pdf files in Loud Cloud. In your git repository, submit all deliverables as .png files. In your git repository, add the comment “Milestone 1, CST117.”